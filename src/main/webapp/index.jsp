<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
    <link rel="stylesheet" type="text/css" href="styles/main.css"/>
    <%@ include file="header.jsp"%>
        <body>
            <div class="main">
                <div class="main-container">
                    <div class="head">Design your own jewelry</div>
                    <div class="select">select style, gemstones and material</div>
                    <div class="start">
                        <div class="start-icon">
                            <span>START DESIGN</span>
                        </div>
                    </div>
                </div>
            </div>
    <%@ include file="footer.jsp"%>
</body>


