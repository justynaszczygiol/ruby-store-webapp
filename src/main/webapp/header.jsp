 <head>
 <meta charset="UTF-8"/>
    <title>ruby store</title>
    <p class="logo">
        <a href="index.jsp">
        <img src="styles/pics/ruby.png" alt="ruby store logo"  width="110" height="auto">
    </p>

    <div class="upper-bar">
        <div class="navigation-left">
            <div class="dropdown">
                <button class="dropdown-button">JEWELRY TYPES</button>
                <div class="dropdown-content">
                    <a href="#">RINGS</a>
                    <a href="#">EARRINGS</a>
                    <a href="#">NECKLACE</a>
                    <a href="#">BRACELETS</a>
                </div>
            </div>
            <div class="dropdown">
                <button class="dropdown-button">JEWELRY COLLECTIONS</button>
                <div class="dropdown-content">
                    <a href="style.jsp">ILLUSION</a>
                    <a href="#">CIRCLES</a>
                    <a href="#">GENTLENESS</a>
                    <a href="#">EXCLUSIVE</a>
                </div>
            </div>
        </div>
        <div class="navigation-right">
            <a href="login.jsp" class="menu-buttons">ACCOUNT</a>
            <button class="menu-buttons">CART</button>
        </div>
    </div>
</head>