<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
    <link rel="stylesheet" href="styles/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="styles/main.css"/>
    <link rel="stylesheet" type="text/css" href="styles/customer.css"/>
    <script src="styles/js/bootstrap.min.js"></script>
    <%@ include file="header.jsp"%>
    <body>
        <div class="main">
            <div class="main-container">
                 <div class="row">
                    <div class="col-4 border-right">
                            <div class="d-flex flex-column align-items-center text-center p-3 py-5">
                                <img class="rounded-circle mt-5" width="150px" src="styles/pics/profile.png">
                                <span class="font-weight-bold">${customer.nickname}</span>
                            </div>
                        </div>
                        <div class="col-8 border-right">
                            <div class="p-3 py-5">
                                <div class="d-flex justify-content-between align-items-center mb-3">
                                    <h4 class="head">Profile Details</h4>
                                </div>
                                <form action="/ruby-store-webapp/customer" method="get">
                                        <div class="row mt-3">
                                            <div class="col-md-6">
                                                <label class="labels">First Name</label>
                                                <p type="text" class="form-control"><c:out value="${customer.getLastName()}"/></p>
                                            </div>
                                            <div class="col-md-6">
                                                <label class="labels">Last Name</label>
                                                <p type="text" class="form-control">${customer.getLastName()}</p>
                                            </div>
                                        </div>
                                        <div class="row mt-3">
                                            <div class="col-md-12">
                                               <label class="labels">E-mail</label>
                                               <p type="text" class="form-control">${customer.email}</p>
                                            </div>
                                            <div class="col-md-12">
                                                <label class="labels">Phone Number</label>
                                                <p type="text" class="form-control">${customer.phoneNumber}</p>
                                            </div>
                                            <div class="col-md-12">
                                            <label class="labels">Registration Date</label>
                                            <p type="text" class="form-control">${customer.registrationDate}</p>
                                            </div>
                                <div class="btn btn-primary">
                                    <button class="btn btn-primary" type="button">Log Out</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </div>
                    </div>
                    <%@ include file="footer.jsp"%>
                </body>
            </html>
