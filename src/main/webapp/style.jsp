<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
    <meta charset="UTF-8"/>
    <title>ruby store</title>
    <link rel="stylesheet" type="text/css" href="styles/main.css"/>
    <p class="logo">
        <img src="styles/pics/ruby.png" alt="ruby store logo"  width="100" height="auto">
    </p>

    <div class="upper-bar">
        <div class="navigation-left">
            <div class="dropdown">
                <button class="dropdown-button">JEWELRY TYPES</button>
                <div class="dropdown-content">
                    <a href="#">RINGS</a>
                    <a href="#">EARRINGS</a>
                    <a href="#">NECKLACE</a>
                    <a href="#">BRACELETS</a>
                </div>
            </div>
            <div class="dropdown">
                <button class="dropdown-button">JEWELRY COLLECTIONS</button>
                <div class="dropdown-content">
                    <a href="style.jsp">ILLUSION</a>
                    <a href="#">CIRCLES</a>
                    <a href="#">GENTLENESS</a>
                    <a href="#">EXCLUSIVE</a>
                </div>
            </div>
        </div>
        <div class="navigation-right">
            <button class="menu-buttons">LOGIN</button>
            <button class="menu-buttons">CART</button>
        </div>
    </div>
</head>

 <div class="main">
        <div class="main-container">
           <br>
               <h1>Collection List</h1>
           </br>

            <c:if test="${not empty message}">
                           <div class="alert alert-success">
                               ${message}
                           </div>
                       </c:if>
                       <form action="/employee" method="post" id="employeeForm" role="form" >
                           <input type="hidden" id="idEmployee" name="idEmployee">
                           <input type="hidden" id="action" name="action">
                           <c:choose>
                               <c:when test="${not empty employeeList}">
                   <table class="table">
                       <thead>
                          <tr>
                              <th scope = "col">Type</th>
                              <th scope = "col">Collection</th>
                              <th scope = "col">Description</th>
                              <th scope = "col">Main gem quantity</th>
                              <th scope = "col">Main gem cut</th>
                              <th scope = "col">Main gem size</th>
                              <th scope = "col">Secondary gem quantity</th>
                              <th scope = "col">Secondary gem cut</th>
                              <th scope = "col">Secondary gem size</th>
                              <th scope = "col">Engraver</th>
                          </tr>
                       </thead>
                       <c:forEach var="style" items="${styleList}">
                                                       <c:set var="classSuccess" value=""/>
                                                       <c:if test ="${ == style.collection}">
                                                       <c:set var="classSuccess" value="info"/>
                                                       </c:if>
                       <tbody>

                           <tr class="${classSuccess}">
                               <td>${style.getType()}</td>
                               <td>${style.getCollection()}</td>
                               <td>${style.getDescription()}</td>
                               <td>${style.getMainGemQuantity()}</td>
                               <td></td>
                               <td></td>
                               <td></td>
                               <td></td>
                               <td></td>
                               <td></td>
                           </tr>
                            </c:forEach>
                       </tbody>
                   </table>
                   </c:when>
                    </c:choose>

        </div>
    </div>


<div class="footer">
    <p class="design">design by Justyna Szczygiol</p>
    <ul class="wrapper">
        <li class="icon">
            <span class="tooltip">gitlab</span>
            <a href="https://gitlab.com/justynaszczygiol">
                <span><img src="styles/pics/gitlab.png" alt="gitlab"  width="20" height="auto"></span>
            </a>
        </li>
        <li class="icon">
            <span class="tooltip">linkedin</span>
            <a href="https://www.linkedin.com/in/justyna-szczygio%C5%82/">
                <span><img src="styles/pics/linkdin.png" alt="linkedin"  width="20" height="auto"></span>
            </a>
        </li>
    </ul>
</div>
</html>