<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
    <link rel="stylesheet" href="styles/bootstrap.min.css"/>
    <script src="styles/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="styles/main.css"/>
    <%@ include file="header.jsp"%>
    <body>
        <div class="main">
            <div class="registration">
            <h1 class="head-register">sign in</h1>
                <div class="login-form">
                <form action="/ruby-store-webapp/login" method="post">
                   <div class="mb-3">
                      <input type="text" placeholder="nickname" class="form-control" name="nickname">
                   </div>
                   <div class="mb-3">
                      <input type="password" placeholder="password" class="form-control" name="password">
                   </div>
                   <div class="d-grid gap-2">
                        <input class="btn btn-primary" type="submit" value="sign in">
                        <a href="registration.jsp" class="btn btn-primary" role="button">are you new in here?</a>
                   </div>
                </form>
                </div>
            </div>
        </div>
        <%@ include file="footer.jsp"%>
    </body>