<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
    <link rel="stylesheet" href="styles/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="styles/main.css"/>
    <script src="styles/js/bootstrap.min.js"></script>
    <%@ include file="header.jsp"%>
    <body>
        <div class="main">
            <div class="registration">
                <h1 class="head-register">create new account</h1>
                <div class="register-form">
                    <form action="/ruby-store-webapp/registration" method="post">
                      <div class="mb-3">
                        <input type="text" placeholder="first name" class="form-control" name="first name">
                      </div>
                      <div class="mb-3">
                        <input type="text" placeholder="last name" class="form-control" name="last name">
                      </div>
                      <div class="mb-3">
                        <input type="text" placeholder="nickname" class="form-control" name="nickname">
                      </div>
                      <div class="mb-3">
                        <input type="password" placeholder="password" class="form-control" name="password">
                      </div>
                      <div class="mb-3">
                        <input type="text" placeholder="e-mail" class="form-control" name="email">
                      </div>
                      <div class="mb-3">
                        <input type="text" placeholder="phone number" class="form-control" name="phone number">
                      </div>
                      <div class="mb-3 form-check">
                        <input type="checkbox" class="form-check-input" id="subscription">
                        <label class="form-check-label" for="exampleCheck1">sign up for newsletter</label>
                      </div>
                      <div class="d-grid gap-2">
                         <input class="btn btn-primary" type="submit" value="register">
                         <a href="login.jsp" class="btn btn-primary">already have account?</a>
                      </div>
                    </form>
                </div>
            </div>
        </div>
        <%@ include file="footer.jsp"%>
    </body>

