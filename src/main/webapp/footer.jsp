  <div class="footer">
        <p class="design">design by Justyna Szczygiol</p>
        <ul class="wrapper">
            <li class="icon">
                <span class="tooltip">gitlab</span>
                <a href="https://gitlab.com/justynaszczygiol">
                <span><img src="styles/pics/gitlab.png" alt="gitlab"  width="20" height="auto"></span>
                </a>
            </li>
            <li class="icon">
                <span class="tooltip">linkedin</span>
                <a href="https://www.linkedin.com/in/justyna-szczygio%C5%82/">
                <span><img src="styles/pics/linkdin.png" alt="linkedin"  width="20" height="auto"></span>
                </a>
            </li>
        </ul>
    </div>