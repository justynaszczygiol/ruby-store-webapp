package com.szczygiol.justyna.servlet;


import com.szczygiol.justyna.database.configuration.DbConnectionManager;
import com.szczygiol.justyna.database.content.dao.CustomerDAO;
import com.szczygiol.justyna.database.content.objects.Customer;
import com.szczygiol.justyna.database.content.util.DAOException;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;

@WebServlet (
        name = "CustomerServlet",
        urlPatterns = "/customer"
)
public class CustomerServlet extends HttpServlet {
    Connection connection = null;
    Customer customer = null;

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        long id = Long.parseLong(session.getId());

        try {
            Class.forName("org.postgresql.Driver");
            DbConnectionManager dcm = new DbConnectionManager();
            connection = dcm.getConnection();
            CustomerDAO customerDAO = new CustomerDAO(connection);
            customer = customerDAO.findById(id);
            request.setAttribute("customer", customer);
            request.setAttribute("action", "edit");
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/customer.jsp");
            dispatcher.forward(request, response);
        } catch (ClassNotFoundException | SQLException | DAOException e) {
            throw new RuntimeException(e);
        }
    }
}
