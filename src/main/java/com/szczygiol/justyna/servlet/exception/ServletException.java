package com.szczygiol.justyna.servlet.exception;

import com.szczygiol.justyna.database.content.util.DAOException;

public class ServletException extends Exception {
    public ServletException (String message) {
        super(message);
    }

    public ServletException(String message, DAOException daoException) {
        super(message, daoException);
    }
}
