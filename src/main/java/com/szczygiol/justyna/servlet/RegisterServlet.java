package com.szczygiol.justyna.servlet;

import com.szczygiol.justyna.database.configuration.DbConnectionManager;
import com.szczygiol.justyna.database.content.dao.CustomerDAO;
import com.szczygiol.justyna.database.content.objects.Customer;
import com.szczygiol.justyna.database.content.util.DAOException;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

@WebServlet (
        name = "RegisterServlet",
        urlPatterns = "/registration")

public class RegisterServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        request.getRequestDispatcher("registration.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException {

        String firstName = request.getParameter("first name");
        String lastName = request.getParameter("last name");
        String nickname = request.getParameter("nickname");
        String password = request.getParameter("password");
        String email = request.getParameter("email");
        String phoneNumber = request.getParameter("phone number");

        Customer customer = new Customer(firstName, lastName, nickname, password, email,
                phoneNumber, true);

        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
        DbConnectionManager dcm = new DbConnectionManager();
        Connection connection = null;
        try {
            connection = dcm.getConnection();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        CustomerDAO customerDAO = new CustomerDAO(connection);

        try {
            customerDAO.create(customer);
        } catch (DAOException | SQLException e) {
            e.printStackTrace();
        }
    }
}
