package com.szczygiol.justyna.servlet;

import com.szczygiol.justyna.database.configuration.DbConnectionManager;
import com.szczygiol.justyna.database.content.dao.CustomerDAO;
import com.szczygiol.justyna.database.content.objects.Customer;
import com.szczygiol.justyna.database.content.util.DAOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Logger;

@WebServlet(
        name = "LoginServlet",
        urlPatterns = "/login")
public class LoginServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        request.getRequestDispatcher("login.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

        String nickname = request.getParameter("nickname");
        String password = request.getParameter("password");

        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }

        DbConnectionManager dcm = new DbConnectionManager();
        Connection connection = null;

        try {
            connection = dcm.getConnection();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        CustomerDAO customerDAO = new CustomerDAO(connection);
        Customer customer = null;

        try {
            customer = customerDAO.findByNickname(nickname);
        } catch (DAOException | SQLException e) {
            throw new RuntimeException(e);
        }

        if (customer.getPassword().equals(password)) {
            HttpSession session = request.getSession();
            session.setAttribute("customer", customer);
            try {
                request.getRequestDispatcher("/customer.jsp").forward(request,response);
            } catch (ServletException e) {
                throw new RuntimeException(e);
            }
        } else
            throw new ServletException("Wrong password");
    }

}
