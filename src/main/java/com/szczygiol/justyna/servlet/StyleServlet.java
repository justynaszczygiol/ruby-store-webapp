package com.szczygiol.justyna.servlet;

import com.szczygiol.justyna.database.configuration.DbConnectionManager;
import com.szczygiol.justyna.database.content.dao.StyleDAO;
import com.szczygiol.justyna.database.content.objects.Style;
import com.szczygiol.justyna.database.content.util.DAOException;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

@WebServlet(
        name = "StyleServlet",
        urlPatterns = {"/style"}
)

public class StyleServlet extends HttpServlet {
    DbConnectionManager dcm = new DbConnectionManager();
    Connection connection = dcm.getConnection();
    StyleDAO styleDAO = new StyleDAO(connection);

    public StyleServlet() throws SQLException {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        StyleDAO styleDAO = new StyleDAO(connection);
       String action = request.getParameter("displayAction");
       if (action != null) {
           switch (action) {
           case "displayByCollection":
               searchByCollection(request, response);
               break;
           case "displayByType":
               searchByType(request, response);
               break;
           }
       } else {
           try {
               List <Style> styles = styleDAO.findAll();
               forwardListStyles(request, response, styles);
           } catch (DAOException e) {
               throw new RuntimeException(e);
           } catch (SQLException e) {
               throw new RuntimeException(e);
           }
       }
    }

    private void searchByCollection(HttpServletRequest request, HttpServletResponse response) {
        String collection = request.getParameter("collection");
        Style style = new Style();
        try {
            List<Style> styles = styleDAO.findByCollection(collection);

        } catch (DAOException e) {
            throw new RuntimeException(e);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    private void searchByType(HttpServletRequest request, HttpServletResponse response) {
        String type = request.getParameter("type");
        Style style = new Style();
        try {
            List<Style> styles = styleDAO.findByType(type);

        } catch (DAOException e) {
            throw new RuntimeException(e);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    private void forwardListStyles(HttpServletRequest request, HttpServletResponse response, List styleList)
            throws ServletException, IOException {
        String nextJSP = "style.jsp";
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(nextJSP);
        request.setAttribute("styleList", styleList);
        dispatcher.forward(request, response);
    }
}
