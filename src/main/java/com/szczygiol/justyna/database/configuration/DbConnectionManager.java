package com.szczygiol.justyna.database.configuration;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class DbConnectionManager {
    private String url;
    private String username;
    private String password;
    private Properties properties = new Properties();

    public Connection getConnection() throws SQLException {
        readProperties();
        this.properties.setProperty("user", username);
        this.properties.setProperty("password", password);
        return DriverManager.getConnection(this.url, this.properties);
    }

    private void readProperties() {
        try {
            Properties loaded = new Properties();
            loaded.load(DbInitializerFromScript.class.getResourceAsStream("/databaseProperties"));
            url = loaded.getProperty("url");
            username = loaded.getProperty("username");
            password = loaded.getProperty("password");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
