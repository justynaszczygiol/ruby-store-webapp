package com.szczygiol.justyna.database.content.util;

public interface DataTransferObject {
    long getId();
}
