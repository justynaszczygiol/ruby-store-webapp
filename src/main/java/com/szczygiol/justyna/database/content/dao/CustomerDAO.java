package com.szczygiol.justyna.database.content.dao;

import com.szczygiol.justyna.database.content.objects.Customer;
import com.szczygiol.justyna.database.content.util.DAOException;
import com.szczygiol.justyna.database.content.util.DataAccessObject;

import java.sql.*;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CustomerDAO extends DataAccessObject<Customer> {

    private static final Logger LOGGER = Logger.getLogger(CustomerDAO.class.getName());
    private static final String INSERT = "INSERT INTO customer(first_name, last_name, nickname, password, email, " +
            "phone_number, registration_date, subscription, sum_of_orders, personal_discount) " +
            "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";

    private static final String GET_ONE = "SELECT id, first_name, last_name, nickname, password, email, " +
            "phone_number, registration_date, subscription, sum_of_orders, personal_discount FROM customer WHERE " +
            "id = ?";

    private static final String UPDATE = "UPDATE customer SET first_name = ?, last_name = ?, nickname = ?, password = ?, " +
            "email = ?, phone_number = ?, registration_date = ?, subscription = ?, sum_of_orders = ?, personal_discount = ? WHERE id = ?";

    private static final String DELETE = "DELETE FROM customer WHERE id = ?";

    private static final String FIND_ALL = "SELECT id, first_name, last_name, nickname, password, email, " +
            "phone_number, registration_date, subscription, sum_of_orders, personal_discount FROM customer";

    private static final String FIND_BY_NICKNAME = "SELECT id, first_name, last_name, nickname, password, email, " +
            "phone_number, registration_date, subscription, sum_of_orders, personal_discount FROM customer WHERE " +
            "nickname = ?";


    public CustomerDAO(Connection connection) {
        super(connection);
    }

    @Override
    public Customer findById(long id) throws DAOException, SQLException {
        Customer customer = new Customer();
        try (PreparedStatement statement = this.connection.prepareStatement(GET_ONE)) {
            statement.setLong(1, id);
            try (ResultSet rs = statement.executeQuery()) {
                while (rs.next()) {
                    customer.setId(rs.getLong("id"));
                    customer.setFirstName(rs.getString("first_name"));
                    customer.setLastName(rs.getString("last_name"));
                    customer.setNickname(rs.getString("nickname"));
                    customer.setPassword(rs.getString("password"));
                    customer.setEmail(rs.getString("email"));
                    customer.setPhoneNumber(rs.getString("phone_number"));
                    customer.setRegistrationDate(rs.getTimestamp("registration_date"));
                    customer.setSubscription(rs.getBoolean("subscription"));
                    customer.setSumOfOrders(rs.getDouble("sum_of_orders"));
                    customer.setPersonal_discount();
                }
            }
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, e.toString(), e);
            throw new DAOException("No object of id " + id + " have been found in the customer", e);
        } finally {
            connection.close();
        }
        return customer;
    }

    @Override
    public List<Customer> findAll() throws DAOException, SQLException {
        List<Customer> customers = new ArrayList<>();
        try (PreparedStatement statement = connection.prepareStatement(FIND_ALL)) {
            try (ResultSet rs = statement.executeQuery()) {
                while (rs.next()) {
                    Customer customer = new Customer();
                    customer.setId(rs.getLong("id"));
                    customer.setFirstName(rs.getString("first_name"));
                    customer.setLastName(rs.getString("last_name"));
                    customer.setNickname(rs.getString("nickname"));
                    customer.setPassword(rs.getString("password"));
                    customer.setEmail(rs.getString("email"));
                    customer.setPhoneNumber(rs.getString("phone_number"));
                    customer.setRegistrationDate(rs.getTimestamp("registration_date"));
                    customer.setSubscription(rs.getBoolean("subscription"));
                    customer.setSumOfOrders(rs.getDouble("sum_of_orders"));
                    customer.setPersonal_discount();
                    customers.add(customer);
                }
            }
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, e.toString(), e);
            throw new DAOException("Read data from customer failed", e);
        } finally {
            connection.close();
        }
        return customers;
    }

    public Customer findByNickname(String nickname) throws DAOException, SQLException {
        Customer customer = new Customer();
        try (PreparedStatement statement = this.connection.prepareStatement(FIND_BY_NICKNAME)) {
            statement.setString(1, nickname);
            try (ResultSet rs = statement.executeQuery()) {
                while (rs.next()) {
                    customer.setId(rs.getLong("id"));
                    customer.setFirstName(rs.getString("first_name"));
                    customer.setLastName(rs.getString("last_name"));
                    customer.setNickname(rs.getString("nickname"));
                    customer.setPassword(rs.getString("password"));
                    customer.setEmail(rs.getString("email"));
                    customer.setPhoneNumber(rs.getString("phone_number"));
                    customer.setRegistrationDate(rs.getTimestamp("registration_date"));
                    customer.setSubscription(rs.getBoolean("subscription"));
                    customer.setSumOfOrders(rs.getDouble("sum_of_orders"));
                    customer.setPersonal_discount();
                }
            }
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, e.toString(), e);
            throw new DAOException("No object of nickname " + nickname + " have been found in the customer", e);
        } finally {
            connection.close();
        }
        return customer;
    }

    @Override
    public Customer update(Customer dto) throws DAOException, SQLException {
        Customer customer;
        try (PreparedStatement statement = this.connection.prepareStatement(UPDATE)) {
            statement.setString(1, dto.getFirstName());
            statement.setString(2, dto.getLastName());
            statement.setString(3, dto.getNickname());
            statement.setString(4, dto.getPassword());
            statement.setString(5, dto.getEmail());
            statement.setString(6, dto.getPhoneNumber());
            statement.setTimestamp(7, Timestamp.from(Instant.now()));
            statement.setBoolean(8, dto.isSubscription());
            statement.setDouble(9, dto.getSumOfOrders());
            statement.setInt(10, dto.getPersonalDiscount());
            statement.setLong(11, dto.getId());
            statement.execute();
            customer = this.findById(dto.getId());
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, e.toString(), e);
            throw new DAOException("Updating customer.css failed", e);
        } finally {
            connection.close();
        }
        return customer;
    }

    @Override
    public Customer create(Customer dto) throws DAOException, SQLException {
        try (PreparedStatement statement = this.connection.prepareStatement(INSERT, PreparedStatement.RETURN_GENERATED_KEYS)) {
            statement.setString(1, dto.getFirstName());
            statement.setString(2, dto.getLastName());
            statement.setString(3, dto.getNickname());
            statement.setString(4, dto.getPassword());
            statement.setString(5, dto.getEmail());
            statement.setString(6, dto.getPhoneNumber());
            statement.setTimestamp(7, Timestamp.from(Instant.now()));
            statement.setBoolean(8, dto.isSubscription());
            statement.setDouble(9, dto.getSumOfOrders());
            statement.setInt(10, dto.getPersonalDiscount());
            statement.execute();
            try (ResultSet keys = statement.getGeneratedKeys()) {
                if (keys.next()) {
                    dto.setId(keys.getLong(1));
                }
            }
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, e.toString(), e);
            throw new DAOException("Creating customer failed", e);
        } finally {
            connection.close();
        }
        return dto;
    }

    @Override
    public void delete(long id) throws DAOException, SQLException {
        try (PreparedStatement statement = this.connection.prepareStatement(DELETE)) {
            statement.setLong(1, id);
            statement.execute();
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, e.toString(), e);
            throw new DAOException("Deleting customer failed", e);
        } finally {
            connection.close();
        }
    }
}
