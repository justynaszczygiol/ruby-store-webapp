package com.szczygiol.justyna.database.content.dao;

import com.szczygiol.justyna.database.content.objects.Shipment;
import com.szczygiol.justyna.database.content.util.DAOException;
import com.szczygiol.justyna.database.content.util.DataAccessObject;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ShipmentDAO extends DataAccessObject<Shipment> {
    private static final String INSERT = "INSERT INTO shipment (method, price) VALUES (?, ?)";
    private static final String GET_ONE = " SELECT id, method, price FROM shipment WHERE id = ?";
    private static final String UPDATE = "UPDATE shipment SET method = ?, price = ? WHERE id = ?";
    private static final String DELETE = "DELETE FROM shipment WHERE id = ?";
    private static final String FIND_ALL = "SELECT id, method, price FROM shipment";


    public ShipmentDAO(Connection connection) {
        super(connection);
    }

    @Override
    public Shipment findById(long id) throws DAOException, SQLException {
        Shipment shipment = new Shipment();
        try (PreparedStatement statement = this.connection.prepareStatement(GET_ONE);) {
            statement.setLong(1, id);
            try (ResultSet rs = statement.executeQuery()) {
                while (rs.next()) {
                    shipment.setId(rs.getLong("id"));
                    shipment.setMethod(rs.getString("method"));
                    shipment.setPrice(rs.getDouble("price"));
                }
            }
        } catch (SQLException e) {
            throw new DAOException("No object of id " + id + " have been found in the shipment", e);
        } finally {
            connection.close();
        }
        return shipment;
    }

    @Override
    public List<Shipment> findAll() throws DAOException, SQLException {
        List<Shipment> shipments = new ArrayList<>();
        try (PreparedStatement statement = connection.prepareStatement(FIND_ALL)) {
            try (ResultSet rs = statement.executeQuery()) {
                while (rs.next()) {
                    Shipment shipment = new Shipment();
                    shipment.setId(rs.getLong("id"));
                    shipment.setMethod(rs.getString("method"));
                    shipment.setPrice(rs.getDouble("price"));
                    shipments.add(shipment);
                }
            }
        } catch (SQLException e) {
            throw new DAOException("Read data from shipment failed", e);
        } finally {
            connection.close();
        }
        return shipments;
    }

    @Override
    public Shipment update(Shipment dto) throws DAOException, SQLException {
        Shipment shipment;
        try (PreparedStatement statement = this.connection.prepareStatement(UPDATE)) {
            statement.setString(1, dto.getMethod());
            statement.setDouble(2, dto.getPrice());
            statement.setLong(3, dto.getId());
            statement.execute();
            shipment = this.findById(dto.getId());
        } catch (SQLException e) {
            throw new DAOException("Updating shipment failed", e);
        } finally {
            connection.close();
        }
        return shipment;
    }

    @Override
    public Shipment create(Shipment dto) throws DAOException, SQLException {
        try (PreparedStatement statement = this.connection.prepareStatement(INSERT, PreparedStatement.RETURN_GENERATED_KEYS)) {
            statement.setString(1, dto.getMethod());
            statement.setDouble(2, dto.getPrice());
            statement.execute();
            try (ResultSet keys = statement.getGeneratedKeys()) {
                if (keys.next()) {
                    dto.setId(keys.getLong(1));
                }
            }
        } catch (SQLException e) {
            throw new DAOException("Creating shipment failed", e);
        } finally {
            connection.close();
        }
        return dto;
    }

    @Override
    public void delete(long id) throws DAOException, SQLException {
        try (PreparedStatement statement = this.connection.prepareStatement(DELETE)) {
            statement.setLong(1, id);
            statement.execute();
        } catch (SQLException e) {
            throw new DAOException("Deleting shipment failed", e);
        } finally {
            connection.close();
        }
    }
}
