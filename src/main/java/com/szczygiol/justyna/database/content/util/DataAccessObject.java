package com.szczygiol.justyna.database.content.util;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public abstract class DataAccessObject <T extends DataTransferObject> {
    protected final Connection connection;

    public DataAccessObject(Connection connection){
        super();
        this.connection = connection;
    }

    public abstract T findById(long id) throws DAOException, SQLException;
    public abstract List<T> findAll() throws DAOException, SQLException;
    public abstract T update(T dto) throws DAOException, SQLException;
    public abstract T create(T dto) throws DAOException, SQLException;
    public abstract void delete(long id) throws DAOException, SQLException;
}
