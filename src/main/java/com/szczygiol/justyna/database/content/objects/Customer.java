package com.szczygiol.justyna.database.content.objects;

import com.szczygiol.justyna.database.content.util.DataTransferObject;

import java.sql.Timestamp;

public class Customer implements DataTransferObject {
    private long id;
    private String firstName;
    private String lastName;
    private String nickname;
    private String password;
    private String email;
    private String phoneNumber;
    private Timestamp registrationDate;
    private boolean subscription;
    private double sumOfOrders;
    private int personalDiscount;

    @Override
    public String toString() {
        return  "id: " + id + " |" +
                " first_name: " + firstName + " |" +
                " last_name: " + lastName + " |" +
                " nickname: " + nickname + " |" +
                " password: " + password + " |" +
                " email: " + email + " |" +
                " phone_number: " + phoneNumber + " |" +
                " registrationDate: " + registrationDate + " |" +
                " subscription=: " + subscription + " |" +
                " sumOfOrders: " + sumOfOrders + " |" +
                " personalDiscount: " + personalDiscount + '\n';
    }

    public Customer() {
    }

    public Customer(String firstName, String lastName, String nickname,
                    String password, String email, String phoneNumber, boolean subscription) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.nickname = nickname;
        this.password = password;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.subscription = subscription;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Timestamp getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(Timestamp registrationDate) {
        this.registrationDate = registrationDate;
    }

    public boolean isSubscription() {
        return subscription;
    }

    public void setSubscription(boolean subscription) {
        this.subscription = subscription;
    }

    public double getSumOfOrders() {
        return sumOfOrders;
    }

    public void setSumOfOrders(double sumOfOrders) {
    }

    public int getPersonalDiscount() {
        return personalDiscount;
    }

    public void setPersonal_discount() {
        if (this.sumOfOrders > 1000.00 && this.sumOfOrders < 4999.99) {
            this.personalDiscount = 5;
        } else if (this.sumOfOrders > 5000.00 && this.sumOfOrders < 9999.99) {
            this.personalDiscount = 10;
        } else if (this.sumOfOrders > 10000.00) {
            this.personalDiscount = 15;
        } else {
            this.personalDiscount = 0;
        }
    }
}