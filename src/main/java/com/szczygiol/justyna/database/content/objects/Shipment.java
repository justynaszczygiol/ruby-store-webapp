package com.szczygiol.justyna.database.content.objects;

import com.szczygiol.justyna.database.content.util.DataTransferObject;

public class Shipment implements DataTransferObject {
    private long id;
    private String method;
    private double price;
    private int daysToDeliver;

    @Override
    public String toString() {
        return " id: " + id +  " |" +
                " method: " + method +  " |" +
                " price: " + price + " |" +
                " days_to_deliver: " + '\n';
    }

    public int getDaysToDeliver() {
        return daysToDeliver;
    }

    public void setDaysToDeliver(int daysToDeliver) {
        this.daysToDeliver = daysToDeliver;
    }

    @Override
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
