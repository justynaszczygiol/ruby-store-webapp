package com.szczygiol.justyna.database.content.objects;

import com.szczygiol.justyna.database.content.util.DataTransferObject;

public class Style implements DataTransferObject {
    private long id;
    private String type;
    private String collection;
    private String description;
    private int mainGemQuantity;
    private int secondaryGemQuantity;
    private String mainGemCut;
    private double mainGemSize;
    private String secondaryGemCut;
    private double secondaryGemSize;
    private boolean engraver;
    private double preciousMetalVariable;
    private double mainGemVariable;
    private double secondaryGemVariable;

    @Override
    public String toString() {
        return  "id: " + id + " |" +
                " type: " + type +  " |" +
                " collection: " + collection +  " |" +
                " description: " + description +  " |" +
                " main_gem_quantity: " + mainGemQuantity + " |" +
                " secondary_gem_quantity: " + secondaryGemQuantity + " |" +
                " main_gem_cut: " + mainGemCut +  " |" +
                " main_gem_size: " + mainGemSize + " |" +
                " secondary_gem_cut: " + secondaryGemCut + " |" +
                " secondary_gem_size: " + secondaryGemSize + " |" +
                " engraver: " + engraver + " |" +
                " precious_metal_variable: " + preciousMetalVariable + " |" +
                " main_gem_variable: " + mainGemVariable + " |" +
                " secondary_gem_variable: " + secondaryGemVariable + '\n';
    }

    @Override
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCollection() {
        return collection;
    }

    public void setCollection(String collection) {
        this.collection = collection;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getMainGemQuantity() {
        return mainGemQuantity;
    }

    public void setMainGemQuantity(int mainGemQuantity) {
        this.mainGemQuantity = mainGemQuantity;
    }

    public int getSecondaryGemQuantity() {
        return secondaryGemQuantity;
    }

    public void setSecondaryGemQuantity(int secondaryGemQuantity) {
        this.secondaryGemQuantity = secondaryGemQuantity;
    }

    public String getMainGemCut() {
        return mainGemCut;
    }

    public void setMainGemCut(String mainGemCut) {
        if (getMainGemQuantity() == 0) {
            mainGemCut = null;
        } else {
            this.mainGemCut = mainGemCut;
        }
    }

    public double getMainGemSize() {
        return mainGemSize;
    }

    public void setMainGemSize(double mainGemSize) {
        if (getMainGemQuantity() == 0) {
            mainGemSize = 0;
        } else {
            this.mainGemSize = mainGemSize;
        }
    }

    public String getSecondaryGemCut() {
        return secondaryGemCut;
    }

    public void setSecondaryGemCut(String secondaryGemCut) {
        if (secondaryGemQuantity == 0) {
            secondaryGemCut = null;
        } else {
            this.secondaryGemCut = secondaryGemCut;
        }
    }

    public double getSecondaryGemSize() {
        return secondaryGemSize;
    }

    public void setSecondaryGemSize(double secondaryGemSize) {
        if (secondaryGemQuantity == 0) {
            this.secondaryGemSize = 0;
        } else {
            this.secondaryGemSize = secondaryGemSize;
        }
    }

    public boolean isEngraver() {
        return engraver;
    }

    public void setEngraver(boolean engraver) {
        this.engraver = engraver;
    }

    public double getPreciousMetalVariable() {
        return preciousMetalVariable;
    }

    public void setPreciousMetalVariable(double preciousMetalVariable) {
        this.preciousMetalVariable = preciousMetalVariable;
    }

    public double getMainGemVariable() {
        setMain_gem_variable();
        return mainGemVariable;
    }

    public void setMain_gem_variable() {
        this.mainGemVariable = mainGemQuantity * mainGemSize * 10;
    }

    public double getSecondaryGemVariable() {
        setSecondary_gem_variable();
        return secondaryGemVariable;
    }

    public void setSecondary_gem_variable() {
        this.secondaryGemVariable = secondaryGemQuantity * secondaryGemSize * 10;
    }
}
