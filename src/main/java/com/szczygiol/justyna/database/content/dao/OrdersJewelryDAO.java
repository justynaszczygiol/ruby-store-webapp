package com.szczygiol.justyna.database.content.dao;

import com.szczygiol.justyna.database.content.objects.OrdersJewelry;
import com.szczygiol.justyna.database.content.util.DAOException;
import com.szczygiol.justyna.database.content.util.DataAccessObject;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class OrdersJewelryDAO extends DataAccessObject<OrdersJewelry> {
    private static final String INSERT = "INSERT INTO orders_jewelry (orders_id, jewelry_id) VALUES (?, ?)";
    private static final String GET_ONE = "SELECT id, orders_id, jewelry_id FROM orders_jewelry WHERE id = ?";
    private static final String GET_ONE_BY_ORDER = "SELECT id, orders_id, jewelry_id FROM orders_jewelry " +
            "WHERE orders_id = ?";
    private static final String GET_ONE_BY_JEWELRY = "SELECT id, orders_id, jewelry_id FROM orders_jewelry " +
            "WHERE jewelry_id = ?";
    private static final String FIND_ALL = "SELECT id, orders_id, jewelry_id FROM orders_jewelry";
    private static final String UPDATE = "UPDATE orders_jewelry SET orders_id = ?, jewelry_id = ?";
    private static final String DELETE = "DELETE from orders_jewelry WHERE id = ?";

    public OrdersJewelryDAO(Connection connection) {
        super(connection);
    }

    @Override
    public OrdersJewelry findById(long id) throws DAOException, SQLException {
        OrdersJewelry oj = new OrdersJewelry();
        try (PreparedStatement statement = this.connection.prepareStatement(GET_ONE)) {
            statement.setLong(1, id);
            try (ResultSet rs = statement.executeQuery()) {
                while (rs.next()) {
                    oj.setId(rs.getLong("id"));
                    OrdersDAO ordersDAO = new OrdersDAO(connection);
                    oj.setOrders_id(ordersDAO.findById(rs.getLong("orders_id")));
                    JewelryDAO jewelryDAO = new JewelryDAO(connection);
                    oj.setJewelry_id(jewelryDAO.findById(rs.getLong("jewelry_id")));
                }
            }
        } catch (SQLException e) {
            throw new DAOException("No object of id " + id + " have been found in the orders_jewelry", e);
        } finally {
            connection.close();
        }
        return oj;
    }

    public List<OrdersJewelry> findByOrder(long id) throws DAOException, SQLException {
        List<OrdersJewelry> ojs = new ArrayList<>();
        try (PreparedStatement statement = this.connection.prepareStatement(GET_ONE_BY_ORDER)) {
            statement.setLong(1, id);
            try (ResultSet rs = statement.executeQuery()) {
                while (rs.next()) {
                    OrdersJewelry oj = new OrdersJewelry();
                    oj.setId(rs.getLong("id"));
                    OrdersDAO ordersDAO = new OrdersDAO(connection);
                    oj.setOrders_id(ordersDAO.findById(rs.getLong("orders_id")));
                    JewelryDAO jewelryDAO = new JewelryDAO(connection);
                    oj.setJewelry_id(jewelryDAO.findById(rs.getLong("jewelry_id")));
                    ojs.add(oj);
                }
            }
        } catch (SQLException e) {
            throw new DAOException("No object of orders_id " + id + " have been found in the orders_jewelry", e);
        } finally {
            connection.close();
        }
        return ojs;
    }

    public List<OrdersJewelry> findByJewelry(long id) throws DAOException, SQLException {
        List<OrdersJewelry> ojs = new ArrayList<>();
        try (PreparedStatement statement = this.connection.prepareStatement(GET_ONE_BY_JEWELRY)) {
            statement.setLong(1, id);
            try (ResultSet rs = statement.executeQuery()) {
                while (rs.next()) {
                    OrdersJewelry oj = new OrdersJewelry();
                    oj.setId(rs.getLong("id"));
                    OrdersDAO ordersDAO = new OrdersDAO(connection);
                    oj.setOrders_id(ordersDAO.findById(rs.getLong("orders_id")));
                    JewelryDAO jewelryDAO = new JewelryDAO(connection);
                    oj.setJewelry_id(jewelryDAO.findById(rs.getLong("jewelry_id")));
                    ojs.add(oj);
                }
            }
        } catch (SQLException e) {
            throw new DAOException("No object of jewelry_id " + id + " have been found in the orders_jewelry", e);
        } finally {
            connection.close();
        }
        return ojs;
    }

    @Override
    public List<OrdersJewelry> findAll() throws DAOException, SQLException {
        List <OrdersJewelry> ojs = new ArrayList<>();
        try (PreparedStatement statement = connection.prepareStatement(FIND_ALL)) {
            try (ResultSet rs = statement.executeQuery()) {
                while (rs.next()) {
                    OrdersJewelry oj = new OrdersJewelry();
                    oj.setId(rs.getLong("id"));
                    OrdersDAO ordersDAO = new OrdersDAO(connection);
                    oj.setOrders_id(ordersDAO.findById(rs.getLong("orders_id")));
                    JewelryDAO jewelryDAO = new JewelryDAO(connection);
                    oj.setJewelry_id(jewelryDAO.findById(rs.getLong("jewelry_id")));
                    ojs.add(oj);
                }
            }
        } catch (SQLException e) {
            throw new DAOException("Read data from orders_jewelry failed", e);
        } finally {
            connection.close();
        }
        return ojs;
    }

    @Override
    public OrdersJewelry update(OrdersJewelry dto) throws DAOException, SQLException {
        OrdersJewelry oj;
        try (PreparedStatement statement = this.connection.prepareStatement(UPDATE)) {
            statement.setLong(1, dto.getOrders_id().getId());
            statement.setLong(2, dto.getJewelry_id().getId());
            statement.setLong(2, dto.getId());
            statement.execute();
            oj = this.findById(dto.getId());
        } catch (SQLException e) {
            throw new DAOException("Updating orders_jewelry failed", e);
        } finally {
            connection.close();
        }
        return oj;
    }

    @Override
    public OrdersJewelry create(OrdersJewelry dto) throws DAOException, SQLException {
        try (PreparedStatement statement = this.connection.prepareStatement(INSERT, PreparedStatement.RETURN_GENERATED_KEYS)) {
            statement.setLong(1, dto.getOrders_id().getId());
            statement.setLong(2, dto.getJewelry_id().getId());
            statement.execute();
            try (ResultSet keys = statement.getGeneratedKeys()) {
                if (keys.next()) {
                    dto.setId(keys.getLong(1));
                }
            }
        } catch (SQLException e) {
            throw new DAOException("Creating orders_jewelry failed", e);
        } finally {
            connection.close();
        }
        return dto;
    }

    @Override
    public void delete(long id) throws DAOException, SQLException {
        try (PreparedStatement statement = this.connection.prepareStatement(DELETE)) {
            statement.setLong(1, id);
            statement.execute();
        } catch (SQLException e) {
            throw new DAOException("Deleting orders_jewelry failed", e);
        } finally {
            connection.close();
        }
    }
}
