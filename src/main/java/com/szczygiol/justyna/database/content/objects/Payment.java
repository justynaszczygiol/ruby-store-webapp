package com.szczygiol.justyna.database.content.objects;

import com.szczygiol.justyna.database.content.util.DataTransferObject;

import java.sql.Timestamp;

public class Payment implements DataTransferObject {
    private long id;
    private Timestamp date;
    private String method;
    private String status = "Pending";

    @Override
    public String toString() {
        return  "id: " + id +  " |" +
                " date: " + date +  " |" +
                " method: " + method +  " |" +
                " status: " + status + '\n';
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
