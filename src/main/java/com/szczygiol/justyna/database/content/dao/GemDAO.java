package com.szczygiol.justyna.database.content.dao;

import com.szczygiol.justyna.database.content.objects.Gem;
import com.szczygiol.justyna.database.content.util.DAOException;
import com.szczygiol.justyna.database.content.util.DataAccessObject;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class GemDAO extends DataAccessObject<Gem> {
    private static final String INSERT = "INSERT INTO gem(description, colour, price_variable) VALUES (?, ?, ?)";
    private static final String GET_ONE = "SELECT id, description, colour, price_variable FROM gem WHERE id = ?";
    private static final String UPDATE = "UPDATE gem SET description = ?, colour = ?, price_variable = ? WHERE id = ?";
    private static final String DELETE = "DELETE FROM gem WHERE id = ?";
    private static final String FIND_ALL = "SELECT id, description, colour, price_variable FROM gem";

    public GemDAO(Connection connection) {
        super(connection);
    }

    @Override
    public Gem findById(long id) throws DAOException, SQLException {
        Gem gem = new Gem();
        try (PreparedStatement statement = this.connection.prepareStatement(GET_ONE);) {
            statement.setLong(1, id);
           try (ResultSet rs = statement.executeQuery()) {
               while (rs.next()) {
                   gem.setId(rs.getLong("id"));
                   gem.setDescription(rs.getString("description"));
                   gem.setColour(rs.getString("colour"));
                   gem.setPriceVariable(rs.getDouble("price_variable"));
               }
           }
        } catch (SQLException e) {
            throw new DAOException("No object of id " + id + " have been found in the gem", e);
        } finally {
            connection.close();
        }
        return gem;
    }

    @Override
    public List<Gem> findAll() throws DAOException, SQLException {
        List<Gem> gems = new ArrayList<>();
        try (PreparedStatement statement = connection.prepareStatement(FIND_ALL)) {
            try (ResultSet rs = statement.executeQuery()) {
                while (rs.next()) {
                    Gem gem = new Gem();
                    gem.setId(rs.getLong("id"));
                    gem.setDescription(rs.getString("description"));
                    gem.setColour(rs.getString("colour"));
                    gem.setPriceVariable(rs.getDouble("price_variable"));
                    gems.add(gem);
                }
            }
        } catch (SQLException e) {
            throw new DAOException("Read data from gem failed", e);
        } finally {
            connection.close();
        }
        return gems;
    }

    @Override
    public Gem update(Gem dto) throws DAOException, SQLException {
        Gem gem = new Gem();
        try (PreparedStatement statement = this.connection.prepareStatement(UPDATE);) {
            statement.setString(1, dto.getDescription());
            statement.setString(2, dto.getColour());
            statement.setDouble(3, dto.getPriceVariable());
            statement.setLong(4, dto.getId());
            statement.execute();
            gem = this.findById(dto.getId());
        } catch (SQLException e) {
            throw new DAOException("Updating gem failed", e);
        } finally {
            connection.close();
        }
        return gem;
    }

    @Override
    public Gem create(Gem dto) throws DAOException, SQLException {
        try (PreparedStatement statement = this.connection.prepareStatement(INSERT, PreparedStatement.RETURN_GENERATED_KEYS)) {
            statement.setString(1, dto.getDescription());
            statement.setString(2, dto.getColour());
            statement.setDouble(3, dto.getPriceVariable());
            statement.execute();
            try (ResultSet keys = statement.getGeneratedKeys()) {
                if (keys.next()) {
                    dto.setId(keys.getLong(1));
                }
            }
        } catch (SQLException e) {
            throw new DAOException("Creating gem failed", e);
        } finally {
            connection.close();
        }
        return dto;
    }

    @Override
    public void delete(long id) throws DAOException, SQLException {
        try (PreparedStatement statement = this.connection.prepareStatement(DELETE);) {
            statement.setLong(1, id);
            statement.execute();
        } catch (SQLException e) {
            throw new DAOException("Deleting gem failed", e);
        } finally {
            connection.close();
        }
    }
}

