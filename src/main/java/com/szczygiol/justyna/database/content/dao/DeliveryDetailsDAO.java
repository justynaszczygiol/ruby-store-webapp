package com.szczygiol.justyna.database.content.dao;

import com.szczygiol.justyna.database.content.objects.DeliveryDetails;
import com.szczygiol.justyna.database.content.util.DAOException;
import com.szczygiol.justyna.database.content.util.DataAccessObject;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DeliveryDetailsDAO extends DataAccessObject<DeliveryDetails> {
    private static final String INSERT = "INSERT INTO delivery_details (first_name, last_name, street, house_number, zip_code, city, country, phone_number, email)" +
            "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?);";

    private static final String GET_ONE = "SELECT id, first_name, last_name, street, house_number, zip_code, city, country, phone_number, email " +
            "FROM delivery_details WHERE id = ?";

    private static final String FIND_ALL = "SELECT id, first_name, last_name, street, house_number, zip_code, city, country, phone_number, email " +
            "FROM delivery_details";

    private static final String UPDATE = "UPDATE delivery_details SET first_name = ?, last_name = ?, street = ?, " +
            "house_number = ?, zip_code = ?, city = ?, country = ?, phone_number = ?, email = ? WHERE id = ?";

    private static final String DELETE = "DELETE FROM delivery_details WHERE id = ?";

    public DeliveryDetailsDAO(Connection connection) {
        super(connection);
    }

    @Override
    public DeliveryDetails findById(long id) throws DAOException, SQLException {
        DeliveryDetails dd = new DeliveryDetails();
        try (PreparedStatement statement = this.connection.prepareStatement(GET_ONE)) {
            statement.setLong(1, id);
            try (ResultSet rs = statement.executeQuery()) {
                while (rs.next()) {
                    dd.setId(rs.getLong("id"));
                    dd.setFirstName(rs.getString("first_name"));
                    dd.setLastName(rs.getString("last_name"));
                    dd.setStreet(rs.getString("street"));
                    dd.setHouseNumber(rs.getString("house_number"));
                    dd.setZipCode(rs.getString("zip_code"));
                    dd.setCity(rs.getString("city"));
                    dd.setCountry(rs.getString("country"));
                    dd.setPhoneNumber(rs.getString("phone_number"));
                    dd.setEmail(rs.getString("email"));
                }
            }
        } catch (SQLException e) {
            throw new DAOException("No object of id " + id + " have been found in the delivery_details", e);
        } finally {
            connection.close();
        }
        return dd;
    }

    @Override
    public List<DeliveryDetails> findAll() throws DAOException, SQLException {
        List<DeliveryDetails> details = new ArrayList<>();
        try (PreparedStatement statement = connection.prepareStatement(FIND_ALL)) {
            try (ResultSet rs = statement.executeQuery()) {
                while (rs.next()) {
                    DeliveryDetails dd = new DeliveryDetails();
                    dd.setId(rs.getLong("id"));
                    dd.setFirstName(rs.getString("first_name"));
                    dd.setLastName(rs.getString("last_name"));
                    dd.setStreet(rs.getString("street"));
                    dd.setHouseNumber(rs.getString("house_number"));
                    dd.setZipCode(rs.getString("zip_code"));
                    dd.setCity(rs.getString("city"));
                    dd.setCountry(rs.getString("country"));
                    dd.setPhoneNumber(rs.getString("phone_number"));
                    dd.setEmail(rs.getString("email"));
                    details.add(dd);
                }
            }
        } catch (SQLException e) {
            throw new DAOException("Read data from delivery_details failed", e);
        } finally {
            connection.close();
        }
        return details;
    }

    @Override
    public DeliveryDetails update(DeliveryDetails dto) throws DAOException, SQLException {
        DeliveryDetails dd;
        try (PreparedStatement statement = this.connection.prepareStatement(UPDATE)) {
            statement.setString(1, dto.getFirstName());
            statement.setString(2, dto.getLastName());
            statement.setString(3, dto.getStreet());
            statement.setString(4, dto.getHouseNumber());
            statement.setString(5, dto.getZipCode());
            statement.setString(6, dto.getCity());
            statement.setString(7, dto.getCountry());
            statement.setString(8, dto.getPhoneNumber());
            statement.setString(9, dto.getEmail());
            statement.setLong(10, dto.getId());
            statement.execute();
            dd = this.findById(dto.getId());
        } catch (SQLException e) {
            throw new DAOException("Updating delivery_details failed", e);
        } finally {
            connection.close();
        }
        return dd;
    }

    @Override
    public DeliveryDetails create(DeliveryDetails dto) throws DAOException, SQLException {
        try (PreparedStatement statement = this.connection.prepareStatement(INSERT, PreparedStatement.RETURN_GENERATED_KEYS)) {
            statement.setString(1, dto.getFirstName());
            statement.setString(2, dto.getLastName());
            statement.setString(3, dto.getStreet());
            statement.setString(4, dto.getHouseNumber());
            statement.setString(5, dto.getZipCode());
            statement.setString(6, dto.getCity());
            statement.setString(7, dto.getCountry());
            statement.setString(8, dto.getPhoneNumber());
            statement.setString(9, dto.getEmail());
            statement.execute();
            try (ResultSet keys = statement.getGeneratedKeys()) {
                if (keys.next()) {
                    dto.setId(keys.getLong(1));
                }
            }
        } catch (SQLException e) {
            throw new DAOException("Creating delivery_details failed", e);
        } finally {
            connection.close();
        }
        return dto;
    }

    @Override
    public void delete(long id) throws DAOException, SQLException {
        try (PreparedStatement statement = this.connection.prepareStatement(DELETE)) {
            statement.setLong(1, id);
            statement.execute();
        } catch (SQLException e) {
            throw new DAOException("Deleting delivery_details failed", e);
        } finally {
            connection.close();
        }
    }
}
