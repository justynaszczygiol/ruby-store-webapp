CREATE TABLE "style"(
    "id" SERIAL PRIMARY KEY,
    "type" VARCHAR(40) NOT NULL,
    "collection" VARCHAR(40) NULL,
    "description" VARCHAR(200) NOT NULL,
    "main_gem_quantity" INTEGER NULL,
    "secondary_gem_quantity" INTEGER NULL,
    "main_gem_cut" VARCHAR(10) NULL,
    "main_gem_size" DECIMAL(3,2) NULL,
    "secondary_gem_cut" VARCHAR(10) NULL,
    "secondary_gem_size" DECIMAL(3,2) NULL,
    "engraver" BOOLEAN NOT NULL,
    "precious_metal_variable" DECIMAL(7,2) NOT NULL,
    "main_gem_variable" DECIMAL(7,2) NULL,
    "secondary_gem_variable" DECIMAL(7,2) NULL
);

CREATE TABLE "gem"(
    "id" SERIAL PRIMARY KEY,
    "description" VARCHAR(20) NOT NULL,
    "colour" VARCHAR(20) NOT NULL,
    "price_variable" DECIMAL(7, 2) NOT NULL
);

CREATE TABLE "precious_metal"(
    "id" SERIAL PRIMARY KEY,
    "type" VARCHAR(20) NOT NULL,
    "description" VARCHAR(40) NOT NULL,
    "carats" INTEGER NULL,
    "purity" INTEGER NULL,
    "price_variable" DECIMAL(7, 2) NOT NULL
);

CREATE TABLE "jewelry"(
    "id" SERIAL PRIMARY KEY,
    "description" VARCHAR(200) NOT NULL,
    "style_id" BIGINT NOT NULL,
    "main_gem" BIGINT NULL,
    "secondary_gem" BIGINT NULL,
    "precious_metal" BIGINT NOT NULL,
    "engraver_text" VARCHAR(20) NULL,
    "price" DECIMAL(7, 2) NOT NULL
);

CREATE TABLE "delivery_details"(
    "id" SERIAL PRIMARY KEY,
    "first_name" VARCHAR(20) NOT NULL,
    "last_name" VARCHAR(40) NOT NULL,
    "street" VARCHAR(40) NOT NULL,
    "house_number" VARCHAR(10) NOT NULL,
    "zip_code" VARCHAR(6) NOT NULL,
    "city" VARCHAR(20) NOT NULL,
    "country" VARCHAR(20) NOT NULL,
    "phone_number" VARCHAR(20) NOT NULL,
    "email" VARCHAR(40) NOT NULL
);

CREATE TABLE "shipment"(
    "id" SERIAL PRIMARY KEY,
    "method" VARCHAR(20) NOT NULL,
    "price" DECIMAL(7, 2) NOT NULL,
    "days_to_deliver" INTEGER NOT NULL
);

CREATE TABLE "customer"(
    "id" SERIAL PRIMARY KEY,
    "first_name" VARCHAR(20) NOT NULL,
    "last_name" VARCHAR(40) NOT NULL,
    "nickname" VARCHAR(40) UNIQUE NOT NULL,
    "password" VARCHAR(20) NOT NULL,
    "email" VARCHAR(40) NOT NULL,
    "phone_number" VARCHAR(20) NULL,
    "registration_date" TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL,
    "subscription" BOOLEAN NOT NULL,
    "sum_of_orders" DECIMAL(7, 2) NOT NULL,
    "personal_discount" INTEGER NOT NULL
);

CREATE TABLE "payment"(
    "id" SERIAL PRIMARY KEY,
    "date" TIMESTAMP NOT NULL,
    "method" VARCHAR (20) NOT NULL,
    "status" VARCHAR (20) NOT NULL DEFAULT 'Pending'
);

CREATE TABLE "orders_jewelry"(
    "id" SERIAL PRIMARY KEY,
    "orders_id" BIGINT NOT NULL,
    "jewelry_id" BIGINT NOT NULL
);

CREATE TABLE "orders"(
    "id" SERIAL PRIMARY KEY,
    "order_date" TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL,
    "customer_id" BIGINT NOT NULL,
    "jewelry_price" DECIMAL(7, 2) NOT NULL,
    "jewelry_discounted_price" DECIMAL(7, 2) NOT NULL,
    "total_price" DECIMAL(7, 2) NOT NULL,
    "delivery_details_id" BIGINT NOT NULL,
    "shipment_id" BIGINT NOT NULL,
    "payment_id" BIGINT NOT NULL,
    "status" VARCHAR(20) NOT NULL
);

ALTER TABLE
    "jewelry" ADD CONSTRAINT "jewelry_secondary_gem_foreign" FOREIGN KEY("secondary_gem") REFERENCES "gem"("id");
ALTER TABLE
    "jewelry" ADD CONSTRAINT "jewelry_precious_metal_foreign" FOREIGN KEY("precious_metal") REFERENCES "precious_metal"("id");
ALTER TABLE
    "orders_jewelry" ADD CONSTRAINT "orders_jewelry_jewelry_id_foreign" FOREIGN KEY("jewelry_id") REFERENCES "jewelry"("id");
ALTER TABLE
    "jewelry" ADD CONSTRAINT "jewelry_style_id_foreign" FOREIGN KEY("style_id") REFERENCES "style"("id");
ALTER TABLE
    "jewelry" ADD CONSTRAINT "jewelry_main_gem_foreign" FOREIGN KEY("main_gem") REFERENCES "gem"("id");
ALTER TABLE
    "orders_jewelry" ADD CONSTRAINT "orders_jewelry_order_id_foreign" FOREIGN KEY("orders_id") REFERENCES "orders"("id");
ALTER TABLE
    "orders" ADD CONSTRAINT "orders_customer_id_foreign" FOREIGN KEY("customer_id") REFERENCES "customer"("id");
ALTER TABLE
    "orders" ADD CONSTRAINT "orders_delivery_details_id_foreign" FOREIGN KEY("delivery_details_id") REFERENCES "delivery_details"("id");
ALTER TABLE
    "orders" ADD CONSTRAINT "orders_shipment_id_foreign" FOREIGN KEY("shipment_id") REFERENCES "shipment"("id");
ALTER TABLE
    "orders" ADD CONSTRAINT "orders_payment_id_foreign" FOREIGN KEY("payment_id") REFERENCES "payment"("id");


    INSERT INTO customer (first_name, last_name, nickname, password, email, phone_number, registration_date, subscription, sum_of_orders, personal_discount) VALUES
    ('Walter', 'White', 'walt', 'meta', 'walter.white@gmail.com', '658-721-425', CURRENT_TIMESTAMP, true, 0.0, 0);
    INSERT INTO customer (first_name, last_name, nickname, password, email, phone_number, registration_date, subscription, sum_of_orders, personal_discount) VALUES
    ('Jesse', 'Pinkman', 'pinkmaster', 'pinky', 'pinkmaster.metafactory@gmail.com', '712-412-442', CURRENT_TIMESTAMP, true, 0.0, 0);
    INSERT INTO customer (first_name, last_name, nickname, password, email, phone_number, registration_date, subscription, sum_of_orders, personal_discount) VALUES
    ('Saul', 'Goodman', 'callsaul', 'topsecret', 'you.better.call.saul@lawyer.com', '125-897-475', CURRENT_TIMESTAMP, false, 0.0, 0);

    INSERT INTO delivery_details (first_name, last_name, street, house_number, zip_code, city, country, phone_number, email)
    VALUES ('Skyler', 'White', 'OakyStret', '7', '74-645', 'Albuquerque', 'USA', '745-415-777', 'sweet.sky@mail.com');
    INSERT INTO delivery_details (first_name, last_name, street, house_number, zip_code, city, country, phone_number, email)
    VALUES ('Andrea', 'Cantillo', '77-Street', '16A', '74-645', 'Albuquerque', 'USA', '748-842-449', 'an.can@gmail.com');
    INSERT INTO delivery_details (first_name, last_name, street, house_number, zip_code, city, country, phone_number, email)
    VALUES ('Saul', 'Goodman', 'St. Patrick Street', '104', '74-746', 'Santa Fe', 'USA', '125-897-475', 'you.better.call.saul@lawyer.com');

    INSERT INTO gem (description, colour, price_variable) VALUES ('emerald', 'green', 585.47);
    INSERT INTO gem (description, colour, price_variable) VALUES ('diamond', 'white', 1217.87);
    INSERT INTO gem (description, colour, price_variable) VALUES ('diamond', 'black', 997.54);

    INSERT INTO precious_metal (type, description, carats, purity, price_variable) VAlUES ('gold', 'yellow gold', 18, 925, 325.14);
    INSERT INTO precious_metal (type, description, purity, price_variable) VAlUES ('gold', 'white gold', 750, 118.79);
    INSERT INTO precious_metal (type, description, carats, price_variable) VAlUES ('gold', 'palladium white gold', 14, 449.50);
    INSERT INTO precious_metal (type, description, carats, purity, price_variable) VAlUES ('gold', 'rose gold', 14, 925, 269.88);

    INSERT INTO payment (date, method, status) VALUES (CURRENT_TIMESTAMP, 'BLIK', 'Pending');
    INSERT INTO payment (date, method, status) VALUES (CURRENT_TIMESTAMP, 'PayPal', 'Pending');
    INSERT INTO payment (date, method, status) VALUES (CURRENT_TIMESTAMP, 'PayU', 'Pending');

    INSERT INTO shipment (method, price, days_to_deliver) VALUES ('DHL', 10.99, 1);
    INSERT INTO shipment (method, price, days_to_deliver) VALUES ('InPost', 7.99, 1);
    INSERT INTO shipment (method, price, days_to_deliver) VALUES ('DPD', 9.99, 1);

    INSERT INTO style (type, collection, description, main_gem_quantity, main_gem_cut, main_gem_size, engraver,
    				   precious_metal_variable)
    VALUES ('Necklace', 'Illusion', 'Delicate necklace with three gem stones decoratively surrounded with chosen precious metal.',
    		3, 'Round', 0.11, false, 1.15);
    INSERT INTO style (type, collection, description, main_gem_quantity, main_gem_cut, main_gem_size, engraver,
    				   precious_metal_variable, main_gem_variable)
    VALUES ('Ring', 'Illusion', 'Delicate ring with one gem stone decoratively surrounded with chosen precious metal.',
    		1, 'Round', 0.11, false, 0.8, 1.1);
    INSERT INTO style (type, collection, description, main_gem_quantity, main_gem_cut, main_gem_size, engraver,
    				   precious_metal_variable, main_gem_variable)
    VALUES ('Earrings', 'Illusion', 'Delicate earrings with three gem stones each decoratively surrounded with chosen precious metal.',
    		6, 'Round', 0.11, false, 0.4, 6.6);
    INSERT INTO style (type, collection, description, main_gem_quantity, main_gem_cut, main_gem_size, engraver,
    				   precious_metal_variable, main_gem_variable)
    VALUES ('Bracelet', 'Illusion', 'Delicate bracelet with three gem stones each decoratively surrounded with chosen precious metal.',
    		3, 'Round', 0.11, false, 1, 3.3);
    INSERT INTO style (type, description, main_gem_quantity, main_gem_cut, main_gem_size, engraver,
    				   precious_metal_variable, main_gem_variable)
    VALUES ('Ring', 'Delicate ring in shape of haulm with leaves with 12 gem stones.',
    		12, 'Round', 0.05, true, 0.9, 5);
    INSERT INTO style (type, description, main_gem_quantity, main_gem_cut, main_gem_size, engraver,
    				   precious_metal_variable, main_gem_variable)
    VALUES ('Ring', 'Classic ring with single cushion cut gem stone.',
    		1, 'Cushion', 0.49, true, 0.9, 4.9);
    INSERT INTO style (type, collection, description, main_gem_quantity, main_gem_cut, main_gem_size, engraver,
    				   precious_metal_variable, main_gem_variable)
    VALUES ('Necklace', 'Circles', 'Orginal necklace with two circles and single round cut gem stone.',
    		1, 'Round', 0.11, false, 2, 1.1);
    INSERT INTO style (type, collection, description, main_gem_quantity, main_gem_cut, main_gem_size, engraver,
    				   precious_metal_variable, main_gem_variable)
    VALUES ('Earrings', 'Circles', 'Orginal earings with two circles and single round cut gem stone each.',
    		2, 'Round', 0.11, false, 1.2, 2.2);
    INSERT INTO style (type, collection, description, main_gem_quantity, main_gem_cut, main_gem_size, engraver,
    				   precious_metal_variable, main_gem_variable)
    VALUES ('Bracelet', 'Gentleness', 'Gentle bracelet with single radiant cut gem stone and decorational metal insets.',
    		1, 'Radiant', 0.15, false, 0.8, 1.5);
    INSERT INTO style (type, collection, description, main_gem_quantity, main_gem_cut, main_gem_size, engraver,
    				   precious_metal_variable, main_gem_variable)
    VALUES ('Necklace', 'Gentleness', 'Gentle necklace with single radiant cut gem stone and decorational metal insets.',
    		1, 'Radiant', 0.15, false, 1.2, 1.5);
    INSERT INTO style (type, collection, description, main_gem_quantity, main_gem_cut, main_gem_size, engraver,
    				   precious_metal_variable, main_gem_variable)
    VALUES ('Earrings', 'Gentleness', 'Gentle earrings with single radiant cut gem stone and decorational metal insets.',
    		2, 'Radiant', 0.15, false, 0.8, 3.0);
    INSERT INTO style (type, collection, description, main_gem_quantity, secondary_gem_quantity, main_gem_cut, main_gem_size, secondary_gem_cut, secondary_gem_size, engraver,
    				   precious_metal_variable, main_gem_variable, secondary_gem_variable)
    VALUES ('Necklace', 'Exclusive', 'Exclusive necklace for elegant ladies with single pear cut main gem stone surounded by slight secondary gem stones.',
    		1, 58, 'Radiant', 0.84, 'Round', 0.01, false, 0.8, 8.4, 5.8);


